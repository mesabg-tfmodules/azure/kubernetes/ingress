variable "resource_group_name" {
  type        = string
  description = "Resource group name"
}

variable "kubernetes_cluster_name" {
  type        = string
  description = "Kubernetes version"
}
