resource "helm_release" "external_nginx" {
  name              = "external"

  repository        = "https://kubernetes.github.io/ingress-nginx"
  chart             = "ingress-nginx"
  namespace         = "ingress"
  create_namespace  = true
  version           = "4.8.0"

  values            = [file("${path.module}/values/ingress.yaml")]
}

resource "helm_release" "cert_manager" {
  name              = "cert-manager"

  repository        = "https://charts.jetstack.io"
  chart             = "cert-manager"
  namespace         = "cert-manager"
  create_namespace  = true
  version           = "v1.13.1"

  set {
    name            = "installCRDs"
    value           = "true"
  }
}
